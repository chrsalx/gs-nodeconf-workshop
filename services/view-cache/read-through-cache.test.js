const nock = require('nock')
const {readOrLoadData} = require('./read-through-cache')

const waitForCondition = (condition, timeoutms) =>
    new Promise((resolve, reject) => {
        const check = () => {
            if (condition()) resolve();
            else if ((timeoutms -= 100) < 0) reject(new Error('timed out!'));
            else setTimeout(check, 100);
        };
        setTimeout(check, 100);
    });



const keyOne = 'k1'

const successBody = {
    "id": "978-0641723445",
    "cat": ["book", "hardcover"],
    "name": "The Lightning Thief",
    "author": "Rick Riordan",
    "series_t": "Percy Jackson and the Olympians",
    "sequence_i": 1,
    "genre_s": "fantasy",
    "inStock": true,
    "price": 12.50,
    "pages_i": 384
}

describe('read-through-cache', () => {

    test('should load resources from origin requested ', (done) => {
        const networkRequest = nock('http://localhost:9000').get('/sample.json')
        readOrLoadData(keyOne, 'http://localhost:9000/sample.json')
        networkRequest.reply(200, successBody)
        waitForCondition(() => {
            const data = JSON.parse(readOrLoadData(keyOne, 'http://localhost:9000/sample.json'))
            if (data.author === "Rick Riordan") {
                done()
                return true
            }
            return false;

        }, 5000)

    })
})